import uuid
import goslate


class Book:

    gs = goslate.Goslate()

    def __init__(self, title, star_rating, price, product_description, picture_url):
        self.id = uuid.uuid4()
        self.title = title
        self.star_rating = star_rating
        self.price = price
        self.product_description = product_description
        self.picture_url = picture_url
        self. product_description_es = self.gs.translate(self.product_description, 'es')
        self. product_description_fr = self.gs.translate(self.product_description, 'fr')

    def show_book(self):
        print(f'''
        ID: {self.id}
        Title: {self.title}
        Star rating: {self.star_rating}
        Price: {self.price}
        Description: {self.product_description}
        Description_ES: {self.product_description_es}
        Description_FR: {self.product_description_fr}
        Picture URL: {self.picture_url}
        ''')