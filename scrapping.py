from selenium import webdriver
from selenium.webdriver.common.by import By

from book import Book


def get_books_from_scrapping():
    books = list()
    driver = webdriver.Chrome()
	# driver.implicitly_wait(10)
	# driver.maximize_window()

    try:
        driver.get('http://books.toscrape.com/index.html')
		# driver.implicitly_wait(3)
		# Localizar cuadro de texto
        book_elements = driver.find_elements(by=By.CLASS_NAME, value='product_pod')
        for index in range(len(book_elements)):
            book_element = driver.find_elements(by=By.CLASS_NAME, value='product_pod')[index]		
            h3_element = book_element.find_element(by=By.TAG_NAME, value="h3")
            link_element = h3_element.find_element(by=By.TAG_NAME, value="a")
            title = link_element.get_attribute("title")

            p_star_element = book_element.find_element(by=By.CLASS_NAME, value="star-rating")
            star_rating = p_star_element.get_attribute("class")[12:]

            p_price_element = book_element.find_element(by=By.CLASS_NAME, value='price_color')
            price = p_price_element.text

            div_image_element = book_element.find_element(by=By.CLASS_NAME, value='image_container')
            a_element = div_image_element.find_element(by=By.TAG_NAME, value='a')
            image_url = a_element.get_attribute('href')

            link_element.click()
            # driver.implicitly_wait(3)
            article_element = driver.find_element(by=By.CLASS_NAME, value='product_page')
            p_element = article_element.find_elements(by=By.TAG_NAME, value='p')[3]
            description = p_element.text

            book = Book(title, star_rating, price, description, image_url)
            books.append(book)
            driver.back()

    except Exception as ex:
        print('Error...')
        print(str(ex))
    finally:
        driver.quit()

    return books
